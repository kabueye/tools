#!/bin/sh

echo `date '+%Y-%m-%d %T'` "START"

date=`date '+%Y%m%d'`
year=`echo ${date} | cut -c 1-4`
month=`echo ${date} | cut -c 5-6`

sudo mysqldump --quote-names --skip-lock-tables --single-transaction --flush-logs --master-data=2 database | gzip > /tmp/mysql-dump.sql.${date}.gz
aws s3 cp /tmp/mysql-dump.sql.${date}.gz s3://backup/${year}/${month}/

echo `date '+%Y-%m-%d %T'` "END"

