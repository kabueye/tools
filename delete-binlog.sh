#/bin/sh

echo `date '+%Y-%m-%d %T'` "START"

query="FLUSH LOGS; PURGE MASTER LOGS BEFORE NOW() - INTERVAL 3 HOUR;"
echo "${query}" | sudo mysql

echo `date '+%Y-%m-%d %T'` "END"
