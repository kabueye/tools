# 概要

必要になって作った簡単なツールを公開していくレポジトリです。

## binlog削除

MySQLのbinlogを1日より細かい単位で削除するシェルスクリプトです。定期的にbinlogを削除するためにはmy.cnfでexpire_logs_daysを指定すれば良いのですが、daysの単位でしか指定できないため、binlogが巨大になってしまう場合など困ってしまいます。このスクリプトを走らせると、3時間以上経過したbinlogを削除します。

### 使い方

$ sh delete-binlog.sh

## バックアップ

MySQLをダンプしてバックアップを取るシェルスクリプトです。ダンプした圧縮ファイルはawscliでS3にアップロードします。

### 使い方

sh mysql-dump.sh

Copyright (c) 2016 [kabueye](http://www.kabueye.com/blog/), [hitorigohanmap](http://hitorigohanmap.com), [電子書籍アラート](https://ebookalerts.net)
